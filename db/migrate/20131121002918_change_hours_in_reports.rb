class ChangeHoursInReports < ActiveRecord::Migration
  def change
    change_column :reports, :total_hours, :decimal
  end
end
