class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.time :start_time
      t.time :end_time
      t.string :description
      t.date :day
      t.integer :total_hours

      t.timestamps
    end
  end
end
