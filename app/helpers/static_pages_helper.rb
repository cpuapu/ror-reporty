module StaticPagesHelper
       include ReportsHelper

  def format_time_view t
    t.hour.to_s + ":" + fix_minutes_view(t.min).to_s
  end


  def set_status_approved val
    if val
       "success"
    else
      "warning"
    end
  end

end
