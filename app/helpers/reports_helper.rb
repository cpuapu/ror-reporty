module ReportsHelper

  def fix_minutes_view min
    (min.to_i < 10) ? min.to_s + '0' : min
  end
end
