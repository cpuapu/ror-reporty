module AdminHelper


  def approved_text report
    if report.approved == true
      'رفض'
    else
      'موافقه'
    end
  end



end


class Date
  def day_name
    if self.sunday?
      'الاحد'
    elsif self.monday?
      'الاثنين'
    elsif self.tuesday?
      'الثلاثاء'
    elsif self.wednesday?
      'الاربعاء'
    elsif self.thursday?
      'الخميس'
    elsif self.friday?
      'الجمعه'
    elsif self.saturday?
      'السبت'
    else
      ''
    end
  end
end