module UsersHelper

  def get_permession_name user
    if user.role == 'admin'
      'مدير'
    elsif user.role == 'employee'
      'مستخدم'
    else
      ''
    end
  end

end
