class StaticPagesController < ApplicationController

  #before_action :set_report, only: [:show, :edit, :update, :destroy]


  #before_action :set_static_pages, only: [:index, :user_report]




  def index

    if session[:user_id]
      if ( User.find(session[:user_id]).role == 'admin')
        redirect_to '/admin'
      else
        redirect_to user_report_path
      end


    else
      redirect_to login_path
    end

  end

  def user_report

    #@is_user =

    @is_user = User.find_by!(id: session[:user_id])
    #@total_hours = @is_user.get_total_hours
    @total_hours = @is_user.get_total_approved_hours

    @user_reports = Report.where(user_id: @is_user.id).order("day ASC")

    @user_report_type0 = Report.where(user_id: @is_user.id, meeting_type: 0).order("day ASC") # teach
    @user_report_type1 = Report.where(user_id: @is_user.id, meeting_type: 1).order("day ASC") # meeting
    @user_report_type2 = Report.where(user_id: @is_user.id, meeting_type: 2).order("day ASC") # absce

      @show = false

  rescue ActiveRecord::RecordNotFound
    flash[:notice] = "خطا باسم المستخدم او كلمة المرور"
    redirect_to login_path



  end





end
