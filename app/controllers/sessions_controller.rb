class SessionsController < ApplicationController

  skip_before_action :authorize

  def new
  end

  def create
    user = User.find_by(id_number: params[:id_number])
    if user and user.authenticate(params[:password])
      session[:user_id] = user.id

      # if user is admin .. else..

      if user.role == 'admin'
        redirect_to '/admin'
      elsif user.role == 'employee'
        redirect_to '/user_report'
      else
        redirect_to root_path
      end


    else
      redirect_to root_path, notice: "خطاباسم المستخدم او كلمه المرور"

    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "تم الخروج"
  end
end
