class AdminController < ApplicationController

  before_action :is_admin

  def index

    @current_user = User.find(session[:user_id])

    @users = User.all

  end


  def user_report_list

    @user = User.find(params[:id])

    redirect_to admin_path unless @user

    @user_reports = Report.where(user_id: @user.id).order("day ASC")
    @user_reports_type0 = Report.where(user_id: @user.id, meeting_type: 0).order("day ASC") # teach
    @user_reports_type1 = Report.where(user_id: @user.id, meeting_type: 1).order("day ASC") # meeting
    @user_reports_type2 = Report.where(user_id: @user.id, meeting_type: 2).order("day ASC") # absce

    @show = false

    #@total_hours = @user.get_total_hours.to_s
    @total_hours = @user.get_total_approved_hours.to_s

  end

  def approve_report

    #@rep = params[:approved]  || params[:data][:approved]
    @aprv =  params[:approved]
    @report_id = params[:re_i]
    @user_id = params[:user_id]

    @report = Report.find_by(id: @report_id, user_id: @user_id)

     if @aprv == '1'
       @report.approved = true
     else
       @report.approved = false
     end

    @report.save

    respond_to do |f|
      f.js
    end

  end


end
