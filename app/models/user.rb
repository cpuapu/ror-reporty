class User < ActiveRecord::Base


  has_many :reports

  validates :id_number, :password, :name, :email, :phone, presence: true
  validates :id_number,:email, :phone,  uniqueness: true

  #validates_confirmation_of :password
  has_secure_password


  def get_total_hours
    hours = Report.where(user_id: self.id)

    total = 0
    hours.each do |h|
      total += h.total_hours.to_f
    end
    total
  end

  def get_total_approved_hours
    hours = Report.where(user_id: self.id, approved: true)

    total = 0
    hours.each do |h|
      total += h.total_hours.to_f
    end
    total
  end

end
