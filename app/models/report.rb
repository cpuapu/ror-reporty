class Report < ActiveRecord::Base


  belongs_to :user

  before_save :update_total_hours
  before_save :update_meeting_type

  validates :start_time, :end_time, :day, presence: true


  validates :description, length: { maximum: 255 }


  private
  def update_total_hours

    hours = end_time.hour - start_time.hour
    minutes  = end_time.min - start_time.min

    if ( hours < 0 or minutes < 0)
      self.end_time = self.start_time
      hours = 0
      minutes  = 0
    end

    self.total_hours = hours + (minutes.to_f/60.0)

  end

  def update_meeting_type
    if self.meeting_type == 2
     self.end_time = self.start_time
     self.total_hours = 0
    end
  end

end
