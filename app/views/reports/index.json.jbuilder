json.array!(@reports) do |report|
  json.extract! report, :start_time, :end_time, :description, :day, :total_hours
  json.url report_url(report, format: :json)
end
